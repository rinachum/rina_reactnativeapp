import  React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TextInput,
    Button,
    Image,
    CheckBox,
 }
 from 'react-native';

 export default class Login extends Component {
     render() {
        const {
            container, 
            Picture, 
            image,
            Username,
            Password,
            label,
        } = styles
         return(
            <View style={container}>
                <View style={Picture}>
                    <Image source={require('../image/picture.png')} 
                           style={image} />
                </View>
                <View>
                    <Text style={label}>Username</Text>
                    <TextInput 
                        style={Username}
                    />
                </View>
                <View>
                    <Text style={label} >Password</Text>
                    <TextInput 
                        secureTextEntry={true} 
                        style={Password}  
                    />
                </View>
                <View style={{flexDirection:'row',justifyContent: 'space-between'}}>
                    <View style={{flexDirection:'row'}}>
                        <CheckBox style={{marginTop: 10,}}/>
                        <Text style={{marginTop: 15,}}>Remember Me</Text>
                    </View>
                    <View>
                    <Button 
                        title={'Log in'} 
                     />
                    </View>
                </View>
                <View style={{marginTop: 30,marginBottom: 20,}}>
                    <Text>Register | Lost your password?</Text>
                </View>
                <View>
                    <Text>Back to Codex Sample</Text>
                </View>
            </View>
                 
         )
     }
 }

const styles = StyleSheet.create({
    container: {
        margin: 20,
        marginTop: 5,
    },
    Picture: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    },
    image: {
        width: 80,
        height: 80 ,
        
    },
    Username: {
        borderWidth:1,
        color: '#000',
        padding: 10,
    },
    Password: {
        borderWidth:1,
        color: '#000',
        padding: 10,
        marginBottom: 25,
    },
    label: {
        fontSize: 17,
        fontWeight: 'bold',
        padding: 8,
    },  
});